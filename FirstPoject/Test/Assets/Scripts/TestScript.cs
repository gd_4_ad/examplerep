﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    private void Start()
    {
        Debug.Log("Start()");
    }

    private void Update()
    {
        if (Input.anyKey)
        {
            Debug.Log("Pressed some key");
        }
    }
}
